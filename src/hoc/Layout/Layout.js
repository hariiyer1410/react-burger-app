import React, { Component } from 'react';
import { connect } from 'react-redux';
import Aux from '../Aux';
import classes from './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';

class Layout extends Component {
    state = {
        showSideDrawer: true
    }

    SideDrawerClosedHandler = () => {
        this.setState({ showSideDrawer: false });
    }

    SideDrawerToggleHandler = () => {
        this.setState((prevState) => {
            return { showSideDrawer: !prevState.showSideDrawer }
        });
    }

    render() {
        return ( 
            <Aux>
                <Toolbar 
                    drawerToggleClicked = { this.SideDrawerToggleHandler }
                    isAuthenticated={this.props.isAuthenticated} /> 
                <SideDrawer closed = { this.SideDrawerClosedHandler }
                    open = { this.state.showSideDrawer }
                    isAuthenticated={this.props.isAuthenticated} /> 
                <main className = { classes.Content } > { this.props.children } </main> 
            </Aux>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null 
    };
};

export default connect(mapStateToProps)(Layout);